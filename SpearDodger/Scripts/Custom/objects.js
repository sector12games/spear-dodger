﻿function Point(x, y) {
    this.x = x;
    this.y = y;
}

function Rect(x, y, width, height) {
    this.left = x;
    this.top = y;
    this.width = width;
    this.height = height;

    this.right = this.left + this.width;
    this.bottom = this.top + this.height;
}

function Spear(position, vector, rotationDegrees) {
    this.position = position;
    this.vector = vector;
    this.rotationDegrees = rotationDegrees;
    this.distanceTraveled = 0;

    this.GetBoundingBoxRect = function () {

    };
}

function Champ(image, overlayImage, position) {

    this.image = image;
    this.overlayImage = overlayImage;
    this.position = position;
    this.desiredPosition = position;

    this.GetBoundingBoxRect = function (boundingBoxBuffer) {
        
    };
}