﻿var canvas;
var ctx;

var spearCanvas;
var spearCanvasCtx;
var spearCanvasData;

var akaliImage;
var akaliOverlayImage;
var nidaleeImage;
var circleImage;
var spearImage;
var movementIndicatorImage;

var imagesLoadedCount = 0;
var totalImages = 6;
var circleRadius = 400;

var globalGraphicalScale = 1.0;

var akaliPosition = new Point(500, 500);
var desiredAkaliPosition = new Point(500, 500);
var nidaleePosition = new Point(1050, 250);
var nidaleePositionDegrees = 0;
var nidaleeIsMovingClockwise = true;

var FPS = 60;
var akaliMoveSpeed = 300;
var nidaleeMoveSpeed = 300;
//var spearSpeed = 1300;
var spearSpeed = 300;
var spearRange = 1500;
var pixelsPerGameUnit = .666;

var timeLastFrame = 0;
var timeMovementIndicatorHasBeenVisible = 0;
var movementIndicatorDisplayDuration = 750;
var screenHasBeenClicked = false;

var timeSinceLastSpear = 0;
var spearInterval = 5000;
var currentSpear = null;

var collisionDetected = false;
var collisionOverlayDisplayDuration = 1500;
var timeCollisionOverlayHasBeenVisible = 0;

var spearConnectingSounds = new Array();
var spearThrownSounds = new Array();

// top, right, bottom, left
//var akaliBoundingBoxBuffer = new Array(20, 20, 0, 20);
var akaliBoundingBoxBuffer = new Array(0, 0, 0, 0);

function load() {
    
    // Get references to the canvas elements
    canvas = document.getElementById('arenaCanvas');
    ctx = canvas.getContext('2d');

    // Disable the right-click context menu for the canvas
    $('body').on('contextmenu', '#arenaCanvas', function (e) { return false; });

    // Add a mousedown listener to our canvas
    canvas.onmousedown = mouseDown;

    // Load our images
    akaliImage = new Image();
    akaliImage.src = 'Images/Custom/akali.png';
    akaliImage.onload = imageLoaded;

    akaliOverlayImage = new Image();
    akaliOverlayImage.src = 'Images/Custom/akali_overlay.png';
    akaliOverlayImage.onload = imageLoaded;

    nidaleeImage = new Image();
    nidaleeImage.src = 'Images/Custom/nidalee.png';
    nidaleeImage.onload = imageLoaded;

    circleImage = new Image();
    circleImage.src = 'Images/Custom/circle.png';
    circleImage.onload = imageLoaded;

    spearImage = new Image();
    spearImage.src = 'Images/Custom/spear_flipped2.png';
    spearImage.onload = imageLoaded;

    movementIndicatorImage = new Image();
    movementIndicatorImage.src = 'Images/Custom/movement_indicator.png';
    movementIndicatorImage.onload = imageLoaded;

    // Buffers automatically when created
    spearThrownSounds[0] = new Audio("Content/Sounds/039_nidalee_base_javelintoss_oc_0.wav");
    spearThrownSounds[1] = new Audio("Content/Sounds/040_nidalee_base_javelintoss_oc_0.wav");
    spearThrownSounds[2] = new Audio("Content/Sounds/041_nidalee_base_javelintoss_oc_0.wav");
    spearThrownSounds[3] = new Audio("Content/Sounds/042_nidalee_base_javelintoss_oc_0.wav");

    spearConnectingSounds[0] = new Audio("Content/Sounds/043_nidalee_base_javelintoss_oh_0.wav");
    spearConnectingSounds[1] = new Audio("Content/Sounds/044_nidalee_base_javelintoss_oh_0.wav");
}

/// This must be performed after all of the images have been loaded.
function createSpearCanvas() {
    // Create a "spear canvas". We will rotate our spear image, and draw
    // it onto this. In other words, we rotate only once when the spear is 
    // created instead of for every draw and every collision test.
    spearCanvas = document.createElement("canvas");
    spearCanvas.id = "spearCanvas";
    spearCanvasCtx = spearCanvas.getContext('2d');

    // The spear canvas should be big enough to fit the spear image with
    // any rotation. Use the largest dimension twice.
    if (spearImage.width > spearImage.height) {
        spearCanvas.width = spearImage.width;
        spearCanvas.height = spearImage.width;
    } else {
        spearCanvas.width = spearImage.height;
        spearCanvas.height = spearImage.height;
    }
}

function imageLoaded() {
    // Keep track of how many images have been loaded
    imagesLoadedCount++;

    // Once all images have been loaded, start our game loop
    if (imagesLoadedCount == totalImages) {

        // Load our spear canvas
        createSpearCanvas();

        // Start our loop
        setInterval(function () {
            update();
            draw();
        }, 1000 / FPS);
    }
}

function update() {
    
    // Get the current time
    var date = new Date();

    // If this is our first time executing the game loop, just update the
    // current time and return. We'll start doing real work next frame.
    if (timeLastFrame === 0) {
        timeLastFrame = date.getTime();
        return;
    }

    // Determine how much time has passed between this frame and the previous.
    // We need to do this because we are not guaranteed that the game loop
    // will be called exactly on the interval specified. This will especially
    // be true on slow machines that cannot keep up with a high FPS.
    var currentTime = date.getTime();
    var timeElapsed = currentTime - timeLastFrame;
    timeLastFrame = currentTime;

    // Update the total amount of time our various indicators have been visible
    timeMovementIndicatorHasBeenVisible += timeElapsed;
    timeCollisionOverlayHasBeenVisible += timeElapsed;

    // Update the total amount of time elapsed since our last spear was thrown
    timeSinceLastSpear += timeElapsed;

    // Move akali to the last location the user clicked
    updateAkaliPosition(timeElapsed);

    // Move nidalee in a random direction around the circle
    updateNidaleePosition(timeElapsed);

    // Create a new spear if it's time for nidalee to throw another one
    if (timeSinceLastSpear > spearInterval)
        createNewSpear();

    // Update an existing spears's location (if one currently exists)
    updateSpearPosition(timeElapsed);

    // Check for a collision. If a collision was found, we will display a temporary
    // red overlay over our akali image and remove the spear.
    if (checkForCollision(akaliPosition, akaliImage, akaliBoundingBoxBuffer, currentSpear, spearCanvas, spearCanvasData)) {
        collisionDetected = true;
        currentSpear = null;
        timeCollisionOverlayHasBeenVisible = 0;
        playRandomSound(spearConnectingSounds);
    }
}

function updateAkaliPosition(timeElapsed) {
    // Determine how far to move our akali based on her movement speed and 
    // the time elapsed between this frame and the previous frame.
    var akaliMoveDist = akaliMoveSpeed * pixelsPerGameUnit * (timeElapsed / 1000);
    
    // Determine the vector on which akali will be moving
    var akaliVector = new Point(
        (desiredAkaliPosition.x - akaliPosition.x),
        (desiredAkaliPosition.y - akaliPosition.y));

    // Normalize the movement distance so that we don't move faster when
    // moving diagonally.
    akaliVector = normalize(akaliVector);

    // Move akali, but make sure not to overshoot our final destination
    var xMove = akaliVector.x * akaliMoveDist;
    var yMove = akaliVector.y * akaliMoveDist;
    
    if (xMove > Math.abs(akaliPosition.x - desiredAkaliPosition.x))
        akaliPosition.x = desiredAkaliPosition.x;
    else
        akaliPosition.x += akaliVector.x * akaliMoveDist;
    if (yMove > Math.abs(akaliPosition.y - desiredAkaliPosition.y))
        akaliPosition.y = desiredAkaliPosition.y;
    else
        akaliPosition.y += akaliVector.y * akaliMoveDist;
}

function updateNidaleePosition(timeElapsed) {
    // Determine how far to move our nidalee based on her movement speed and 
    // the time elapsed between this frame and the previous frame.
    var nidaleeMoveDist = nidaleeMoveSpeed * pixelsPerGameUnit * (timeElapsed / 1000);

    // Determine the circumference of our circle (diameter * pi)
    var circumference = 2 * circleRadius * Math.PI;

    // Determine the pixel distance for one degree 
    var pixelsPerDegree = circumference / 360;

    // Determine how many degrees to move nidalee
    var degreesToMove = nidaleeMoveDist / pixelsPerDegree;

    // If nidalee is currently moving counter-clockwise, subtract
    if (!nidaleeIsMovingClockwise)
        degreesToMove *= -1;

    // Update nidalee's position around the circle
    nidaleePositionDegrees += degreesToMove;

    // Make sure to keep her position between 0 and 360
    if (nidaleePositionDegrees > 360)
        nidaleePositionDegrees -= 360;
    if (nidaleePositionDegrees < 0)
        nidaleePositionDegrees += 360;

    // Finally, update nidalee's drawing coordinates
    nidaleePosition = getCoordinatesOnCircle(circleRadius, nidaleePositionDegrees);
}

function updateSpearPosition(timeElapsed) {
    // If we don't currently have a spear in the air, don't update 
    if (!currentSpear)
        return;

    // Determine how far to move our spear based on it's movement speed and 
    // the time elapsed between this frame and the previous frame.
    var spearMoveDist = spearSpeed * pixelsPerGameUnit * (timeElapsed / 1000);

    // Move the spear
    currentSpear.position.x += currentSpear.vector.x * spearMoveDist;
    currentSpear.position.y += currentSpear.vector.y * spearMoveDist;

    // Update the total distance that this spear has traveled
    currentSpear.distanceTraveled += spearMoveDist;

    // If the spear has moved farther than its max range, remove it
    if (currentSpear.distanceTraveled > spearRange)
        currentSpear = null;
}

function createNewSpear() {

    // Reset our spear variables
    timeSinceLastSpear = 0;
    collisionDetected = false;

    // Create a new spear at nidalee's current position
    var position = new Point(nidaleePosition.x, nidaleePosition.y);
    
    // Determine the vector based on where nidalee and akali are
    // currently positioned. This is only determined once - the spear
    // will continue to move straight after it is thrown.
    var vector = new Point(
        (akaliPosition.x - nidaleePosition.x),
        (akaliPosition.y - nidaleePosition.y));

    // Normalize the vector so that the spear doesn't move faster when 
    // moving diagonally.
    vector = normalize(vector);

    // Calculate the spear's rotation angle;
    var rotationDegrees = calculateAngleInDegrees(akaliPosition, nidaleePosition);

    // Create the spear object
    currentSpear = new Spear(
        position,
        vector,
        rotationDegrees
    );

    // Clear our spear canvas
    spearCanvasCtx.clearRect(0, 0, spearCanvas.width, spearCanvas.height);

    // Draw the rotated spear onto its own canvas. We can then use this
    // canvas to draw and perform collision detection without rotating.
    // Draw it in the center of the canvas.
    drawImageRotated(
        spearCanvasCtx,
        spearImage,
        spearCanvas.width / 2 - spearImage.width / 2,
        spearCanvas.height / 2 - spearImage.height / 2,
        spearImage.width,
        spearImage.height,
        currentSpear.rotationDegrees);

    // Get the pixel data for our new spear canvas. We'll need this later
    // for performing collision detection.
    spearCanvasData = spearCanvasCtx.getImageData(0, 0, spearCanvas.width, spearCanvas.height).data;

    // Play the "spear thrown" sound
    playRandomSound(spearThrownSounds);
}

function draw() {

    //ctx.translate(canvas.width / 2, canvas.height / 2);

    // scale y component
    //ctx.scale(1, 0.5);
    //ctx.scale(globalGraphicalScale, globalGraphicalScale);

    var opacityPercent;

    // Adjust the canvas size according to scale
    canvas.width = circleImage.width;
    canvas.height = circleImage.height;

    // Draw the background circle
    drawImageScaled(ctx, circleImage, 0, 0);

    // Draw our movement indicator. Our movement indicator will only be
    // drawn for a short period of time, and will then quickly fade out.
    // If another mouse click is processed before the previous click has
    // fully faded out, we will remove the old one immediately.
    if (screenHasBeenClicked && timeMovementIndicatorHasBeenVisible < movementIndicatorDisplayDuration) {
        opacityPercent = (movementIndicatorDisplayDuration - timeMovementIndicatorHasBeenVisible) / movementIndicatorDisplayDuration;
        ctx.globalAlpha = opacityPercent;
        drawImageScaled(
            ctx,
            movementIndicatorImage,
            desiredAkaliPosition.x - (movementIndicatorImage.width / 2),
            desiredAkaliPosition.y - (movementIndicatorImage.height / 2));
        ctx.globalAlpha = 1.0;
    }

    // Draw our akali image
    drawImageScaled(
        ctx,
        akaliImage,
        akaliPosition.x - (akaliImage.width / 2),
        akaliPosition.y - akaliImage.height);

    // If akali has recently been hit by a spear, draw a red overlay on top of
    // her, to give a visual indication that she was hit
    if (collisionDetected && timeCollisionOverlayHasBeenVisible < collisionOverlayDisplayDuration) {
        opacityPercent = (collisionOverlayDisplayDuration - timeCollisionOverlayHasBeenVisible) / collisionOverlayDisplayDuration;
        ctx.globalAlpha = opacityPercent;
        drawImageScaled(
            ctx,
            akaliOverlayImage,
            akaliPosition.x - akaliOverlayImage.width / 2,
            akaliPosition.y - akaliOverlayImage.height);
        ctx.globalAlpha = 1.0;
    }

    // Draw our nidalee image
    drawImageScaled(
        ctx,
        nidaleeImage,
        nidaleePosition.x - (nidaleeImage.width / 2),
        nidaleePosition.y - (nidaleeImage.height / 2));

    // Draw the current spear, if one exists
    if (currentSpear) {
        drawImageScaled(
            ctx,
            spearCanvas,
            currentSpear.position.x - spearCanvas.width / 2,
            currentSpear.position.y - spearCanvas.height / 2);
    }
}

/// http://stackoverflow.com/questions/2677671/how-do-i-rotate-a-single-object-on-an-html-5-canvas
function drawImageRotated(ctx, img, x, y, width, height, deg) {
    ctx.save();

    //Convert degrees to radian 
    var rad = deg * Math.PI / 180;

    //Set the origin to the center of the image
    ctx.translate(x + width / 2, y + height / 2);

    //Rotate the canvas around the origin
    ctx.rotate(rad);

    //draw the image    
    drawImageScaled(
        ctx,
        img,
        width / 2 * (-1),
        height / 2 * (-1));

    //reset the canvas  
    ctx.rotate(rad * (-1));
    ctx.translate((x + width / 2) * (-1), (y + height / 2) * (-1));

    ctx.restore();
}

function drawImageScaled(ctx, image, x, y) {
    ctx.drawImage(
        image,
        x,
        y,
        image.width * globalGraphicalScale,
        image.height * globalGraphicalScale);
}

function mouseDown(evt) {
    // Determine the point on the canvas that was clicked. This will
    // be the point that we'll start moving akali towards next frame.
    desiredAkaliPosition = getCursorPos(canvas, evt);

    timeMovementIndicatorHasBeenVisible = 0;
    screenHasBeenClicked = true;
}
