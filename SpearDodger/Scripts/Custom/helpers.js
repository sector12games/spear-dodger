﻿/// Expects an array of sounds as input. One of the sounds will be randomly
/// chosen and played.
/// http://www.reddit.com/r/leagueoflegends/comments/1hg433/league_of_legends_sounds_extracted_from_game/
/// http://wiki.spectralcoding.com/info:lol_sound_pages:nidalee_-_default
function playRandomSound(soundsArray) {
    // Determine how many sounds we have to choose from. 
    var count = soundsArray.length;

    // Randomly choose one 
    var random = getRandomNumber(0, count - 1);

    // Play the sound. If it is currently playing (this can happen when spears are 
    // being thrown very fast), reset it to the beginning and play it again.
    soundsArray[random].currentTime = 0;
    soundsArray[random].play();
}

///
/// http://stackoverflow.com/questions/3594177/random-number-between-10-and-10-in-javascript
function getRandomNumber(min, max) {
    var random = Math.floor(Math.random() * (max - min + 1)) + min;
    return random;
}

function normalize(point) {
    // Determine the length of the vector (a^2 + b^2 = c^2)
    var length = Math.pow(point.x, 2) + Math.pow(point.y, 2);
    length = Math.sqrt(length);

    // Now devide both the x and the y by the length we found.
    // Basically, we are changing the length to be 1.
    var newX = point.x / length;
    var newY = point.y / length;

    if (isNaN(newX))
        newX = 0;
    if (isNaN(newY))
        newY = 0;

    // Return the normalized vector
    return new Point(newX, newY);
}

/// Calculates the angle between the horizontal x axis and the line that is joined
/// by these two points. Here are some rules to keep in mind when working with this:
/// - Point1 is assumed to be on the horizontal x axis.
/// - The result of this function will always be between 0 and +-180, (not 360)
/// - Rotating counterclockwise will give a negative angle
///     - This happens when Point2 is above Point1
/// - Rotating clockwise will give a positive result
///     - This happens when Point2 is below Point1
/// http://www.gamedev.net/topic/318640-how-to-get-angle-between-two-points/
/// http://stackoverflow.com/questions/7586063/how-to-calculate-the-angle-between-two-points-relative-to-the-horizontal-axis
/// http://www.youtube.com/watch?v=xLVzTa0NZFI
function calculateAngleInDegrees(point1, point2) {
    var result = Math.atan2(point2.y - point1.y, point2.x - point1.x);
    result *= 180 / Math.PI;
    return result;
}

///
/// http://stackoverflow.com/questions/5300938/calculating-the-position-of-points-in-a-circle
function getCoordinatesOnCircle(radius, angleInDegrees) {
    // Convert degrees to radians
    var angleInRadians = angleInDegrees * (Math.PI / 180);

    // Find the center of the circle (h,k) (the center of our canvas)
    var h = canvas.width / 2;
    var k = canvas.height / 2;

    // Find the x and y coordinates
    var x = radius * Math.cos(angleInRadians) + h;
    var y = radius * Math.sin(angleInRadians) + k;

    // Return the (x,y) point that exists on the circle
    return new Point(x, y);
}

///
/// http://stackoverflow.com/questions/5085689/tracking-mouse-position-in-canvas
function getCursorPos(obj, evt) {
    var curleft = 0, curtop = 0;
    var resultX;
    var resultY;
    if (obj.offsetParent) {
        do {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);

        resultX = evt.pageX - curleft;
        resultY = evt.pageY - curtop;

        // For whatever reason, offsets values are different in firefox and chrome.
        // I can't find any concrete information on the subject, but other people
        // have had these problems as well. Anyway, by manually running through the
        // recursive do/while loop above in the chrome debugger and firebug I managed 
        // to see that the difference SHOULD be about 24 pixels. However, 24 still 
        // looked a bit off, so I pushed the numbers up and down by a few, and 21
        // looks to be the magic number. It's possible that in the future firefox
        // will correct this issue, or my html code will change in some unknown way
        // so the issue no longer occurs. If that is the case, comment this hack out.
        // There's a little more information in the link below.
        // http://stackoverflow.com/questions/8452501/jquery-mouse-offset-position-different-in-firefox-and-chrome
        //console.log("curleft: " + curleft + ", curtop: " + curtop);
        //if ($.browser.mozilla)
        //    resultY -= 21;

        return { x: resultX, y: resultY };
    }
    return undefined;
}

/// Rounds all rect values. This is useful for when we don't want to deal with
/// partial pixel values.
function roundRectValues(rect) {
    
    // Round all coordinates
    var left = Math.round(rect.left);
    var right = Math.round(rect.right);
    var top = Math.round(rect.top);
    var bottom = Math.round(rect.bottom);

    // Create the new, rounded rect
    var result = new Rect(
        left,
        top,
        right - left,
        bottom - top);

    return result;
}