﻿/// Remember that our spear is rotated, but our spearCanvas is not. We'll use the
/// image pixel data from the spearCanvas to check for collision without having
/// to take rotation into account.
///
/// This is a semi-pixel perfect collision detection. We're checking the spear
/// using a pixel perfect method, and we're checking the akali image using a
/// bounding box. Essentially, we're checking to see if any spear pixels are
/// found within akali's bounding box.
function checkForCollision(akaliPosition, akaliImage, akaliBoundingBoxBuffer, currentSpear, spearCanvas, spearCanvasData) {

    // If the spear is currently null or undefined, there is no collision
    if (!currentSpear)
        return false;

    // Determine the bounding box for akali. She will have a small buffer on the 
    // top and sides. The boundingBoxBuffer array is: (top, right, bottom, left)
    var akaliRect = new Rect(
        akaliPosition.x - akaliImage.width / 2 + akaliBoundingBoxBuffer[3],
        akaliPosition.y - akaliImage.height + akaliBoundingBoxBuffer[0],
        akaliImage.width - akaliBoundingBoxBuffer[3] - akaliBoundingBoxBuffer[1],
        akaliImage.height - akaliBoundingBoxBuffer[0] - akaliBoundingBoxBuffer[2]);

//    var akaliRect = new Rect(
//        akaliPosition.x - akaliImage.width / 2 + akaliBoundingBoxBuffer[3],
//        akaliPosition.y - akaliImage.height / 2 + akaliBoundingBoxBuffer[0],
//        akaliImage.width - akaliBoundingBoxBuffer[3] + akaliBoundingBoxBuffer[1],
//        akaliImage.height - akaliBoundingBoxBuffer[0] + akaliBoundingBoxBuffer[2]);

    // The spear's bounding box is simply the spear image itself. It has no buffer.
    var spearRect = new Rect(
        currentSpear.position.x - spearCanvas.width / 2,
        currentSpear.position.y - spearCanvas.height / 2,
        spearCanvas.width,
        spearCanvas.height);

    // First, we need to determine where the akali bounding box and the spear image
    // are overlapping. We only need to check the area where the two squares overlap
    // for collisions.
    var overlapRect = getRectOverlapArea(akaliRect, spearRect);

    // Now calculate the top-left corner of the overlap rect RELATIVE TO our 
    // spear object. The previous overlapRect was relative to our primary canvas.
    var spearOverlapRect = new Rect(
        overlapRect.left - spearRect.left,
        overlapRect.top - spearRect.top,
        overlapRect.width,
        overlapRect.height);

    // Make sure to round all of our results. We don't want to be dealing
    // with partial pixels when it comes to collision detection
    spearOverlapRect = roundRectValues(spearOverlapRect);

    // Next, iterate over every pixel in the pixel data. If there is any color
    // detected on any pixel, it counts as a collision.
    for (var i = spearOverlapRect.left; i < spearOverlapRect.right; i++) {
        for (var j = spearOverlapRect.top; j < spearOverlapRect.bottom; j++) {
            
            // The image data is a flat array
            var index = i * j + i;

            // Each pixel actually takes up 4 slots in the array
            index = index * 4;

            // Get the rgba values from the data array
            var r = spearCanvasData[index];
            var g = spearCanvasData[index + 1];
            var b = spearCanvasData[index + 2];
            var a = spearCanvasData[index + 3];

            // Check for color
            if (a > 0 && (r > 0 || g > 0 || b > 0))
                return true;
        }
    }

    // The overlap rects were fully checked, and no collisions were found.
    return false;
}

/// Creates a rectangle defining the area where one rectangle overlaps 
/// with another rectangle.
/// http://math.stackexchange.com/questions/99565/simplest-way-to-calculate-the-intersect-area-of-two-rectangles
/// http://jsfiddle.net/uthyZ/
function getRectOverlapArea(r1, r2) {

    // This function will not work when rectangles are not overlapping at all.
    // The math will come out with negatives. Always check to make sure the
    // rects are indeed overlapping.
    if (!areRectsOverlapping(r1, r2))
        return new Rect(0, 0, 0, 0);

    // This will also work when one rect is completely contained within the other
    var left = Math.max(r1.left, r2.left);
    var right = Math.min(r1.right, r2.right);
    var top = Math.max(r1.top, r2.top);
    var bottom = Math.min(r1.bottom, r2.bottom);

    var result = new Rect(
        left,
        top,
        right - left, 
        bottom - top);

    return result;
}

///
/// http://stackoverflow.com/questions/2752349/fast-rectangle-to-rectangle-intersection
function areRectsOverlapping(r1, r2) {
    return !(
        r2.left > r1.right ||
        r2.right < r1.left ||
        r2.top > r1.bottom ||
        r2.bottom < r1.top);
}
